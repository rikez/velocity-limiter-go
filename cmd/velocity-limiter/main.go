package main

import (
	"crypto/md5"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/comms/transactionevalevent"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/comms/transactionevent"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/repository"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/service"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/task"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/transactionlimiter"
	"gitlab.com/rikez/velocity-limiter-go.git/pkg/pubsub"
)

func init() {
	logrus.SetFormatter(&logrus.TextFormatter{})
	logrus.SetLevel(logrus.InfoLevel)
}

func main() {
	defer catchPanic()

	// get env vars config
	cfg, err := getConfig()
	if err != nil {
		logrus.Panicf("failed to get initialization config: %v", err)
	}

	outputFilePath := filepath.Join(cfg.OutputDirPath, time.Now().Format("out_20060102150304.000.log"))

	outputFile, err := os.Create(outputFilePath)
	if err != nil {
		logrus.Panicf("failed to create output file: %v", err)
	}
	defer outputFile.Close()

	// Apply rules to limit transactions
	transactionLimiter := transactionlimiter.NewInMemory(cfg.TransactionLimiter)
	// In Memory transactions storage
	inMemoryRepository := repository.NewInMemory()

	// An in memory pubsub implementation
	pubsubClient := pubsub.NewPubSub(cfg.Pubsub)

	// Event handlers for evaluated transactions
	transactionEvalEventHandler := transactionevalevent.NewHandler(outputFile)
	transactionEvalEventConsumer := transactionevalevent.NewConsumer(cfg.TransactionEvalEvent.Consumer, pubsubClient, transactionEvalEventHandler)
	transactionEvalEventConsumer.Consume()
	transactionEvalEventPublisher := transactionevalevent.NewProducer(cfg.TransactionEvalEvent.Producer, pubsubClient)

	// Transactions core logic
	transactionSvc := service.NewService(transactionEvalEventPublisher, transactionLimiter, inMemoryRepository)

	// Event handlers for the new transactions
	transactionEventHandler := transactionevent.NewHandler(transactionSvc)
	transactionEventConsumer := transactionevent.NewConsumer(cfg.TransactionEvent.Consumer, pubsubClient, transactionEventHandler)
	transactionEventConsumer.Consume()
	transactionEventPublisher := transactionevent.NewProducer(cfg.TransactionEvent.Producer, pubsubClient)

	// Task to process the input file
	transactionProcessorTask, err := task.NewTransactionProcessorTask(cfg.TransactionProcessor, transactionEventPublisher)
	if err != nil {
		logrus.Panicf("failed to instantiate a new transaction processor: %v", err)
	}

	if err := transactionProcessorTask.Start(); err != nil {
		logrus.Panicf("unexpected error on transaction processor: %v", err)
	}

	logrus.Infof("Generated output file at %s", outputFilePath)

	// Checks if there are differences between the generated file and the expected file
	if err := diff(cfg.ExpectedFilePath, outputFilePath); err != nil {
		logrus.Errorf("Generated output file[%s] generated doesn't match the expected[%s]: %v", outputFilePath, cfg.ExpectedFilePath, err)
		return
	}

	logrus.Infof("Output file[%s] matches the expected file[%s]", outputFilePath, cfg.ExpectedFilePath)
}

func diff(expectedFilePath, actualFilePath string) error {
	expectedFile, err := ioutil.ReadFile(expectedFilePath)
	if err != nil {
		return err
	}
	actualFile, err := ioutil.ReadFile(actualFilePath)
	if err != nil {
		return err
	}

	hashExpected := md5.Sum(expectedFile)
	hashActual := md5.Sum(actualFile)

	if string(hashExpected[:]) != string(hashActual[:]) {
		return errors.New("unexpected output generated")
	}
	return nil
}

func catchPanic() {
	if err := recover(); err != nil {
		logrus.Errorf("Panic recovered: %v", err)
		panic(err)
	}
}
