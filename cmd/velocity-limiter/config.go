package main

import (
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/comms"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/task"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/transactionlimiter"
	"gitlab.com/rikez/velocity-limiter-go.git/pkg/pubsub"
)

type config struct {
	// Pubsub pubsub related config
	Pubsub pubsub.Config
	// TransactionLimiter provides instrumented config to set up the criteria to limit transactions per customer
	TransactionLimiter transactionlimiter.InMemoryConfig
	// TransactionProcessor provides instrumented config to process an input
	TransactionProcessor task.TransactionProcessorConfig
	// TransactionEvent are variables related to the Transaction Event
	TransactionEvent event
	// TransactionEvalEvent are variables related to the Transaction Eval Event
	TransactionEvalEvent event
	// The file you would like to compare the responses to
	ExpectedFilePath string `required:"true"`
	// The directory to generate the output file
	OutputDirPath string `required:"true"`
}

type event struct {
	Producer comms.ProducerConfig
	Consumer comms.ConsumerConfig
}

func getConfig() (cfg config, err error) {
	cfg = config{}
	err = envconfig.Process("velocitylimiter", &cfg)

	return
}
