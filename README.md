## About

Please refer to the [Service](./SERVICE.md) section.

## Challenge

Please refer to the [Challenge](./CHALLENGE.md) section

## Getting Started

### Requirements

- go 1.14+
- golangci-lint 1.27.0
- mockery 2.0.3

### Installation

#### Check Tools

In order to make all the checks in this codebase, there are some external go tools that must be installed:

```bash
$ make install-tools
```

#### Project Dependencies

```bash
$ make install-deps
```

## Building

Note: if you are using Mac, you need to make sure you have the GNU coreutils instead of the default BSD. You can install 
these using brew:
```bash
$ brew install parallel coreutils findutils grep bash
```

To use the latest `bash` version remember to set: `export PATH=/usr/local/bin:$PATH`

### Compile & Build Docker image

We can compile and run the service using docker.

To build the service run:

```bash
$ make compile build
```

This will compile the `velocity-limiter` service in a container and create the image.

## Testing & Linting

It will run go linters and the unit tests
```bash
$ make check
```

If you prefer to run the test separately
```bash
$ make test
```

Or even if you prefer to run unit tests and visualize a detailed coverage report
```bash
$ make coverage
```

## Start app

### Environment variables

Using `envconfig` lib to fetch and validate env vars

```bash
VELOCITYLIMITER_TRANSACTIONPROCESSOR_PATH=test/testdata/transactions_input.txt # Path to the input file
VELOCITYLIMITER_EXPECTEDFILEPATH=test/testdata/transactions_output_expected.txt # File path to the expected output (used to diff the generated one)
VELOCITYLIMITER_TRANSACTIONEVENT_CONSUMER_TOPIC=transactions-event-v1 # Topic name to consume transaction events
VELOCITYLIMITER_TRANSACTIONEVENT_PRODUCER_TOPIC=transactions-event-v1 # Topic name to produce transaction events
VELOCITYLIMITER_TRANSACTIONEVALEVENT_PRODUCER_TOPIC=transaction-evaluated-event-v1 # Topic name to consume transaction eval events
VELOCITYLIMITER_TRANSACTIONEVALEVENT_CONSUMER_TOPIC=transaction-evaluated-event-v1 # Topic name to produce transaction eval events
VELOCITYLIMITER_OUTPUTDIRPATH=out/ # Directory to store the output
VELOCITYLIMITER_TRANSACTIONLIMITER_MAXAMOUNTPERDAY=5000.0 # Optional (default: 5000.0)
VELOCITYLIMITER_TRANSACTIONLIMITER_MAXAMOUNTPERWEEK=20000.0 # Optional (default: 20000.0)
VELOCITYLIMITER_TRANSACTIONLIMITER_MAXLOADSPERDAY=3 # Optional (default: 3)
```

Verify if the environment variables at `scripts/local/start.sh` are configured accordingly to your needs and then

```bash
$ make start
```