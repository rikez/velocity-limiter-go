#!/bin/bash

# Execute project locally

# Check required commands are in place
command -v go > /dev/null 2>&1 || { echo 'please install go or use image that has it'; exit 1; }

echo -e "\n# Starting distribution: $1\n"
## Compile and run project
VELOCITYLIMITER_TRANSACTIONPROCESSOR_PATH=test/testdata/transactions_input.txt \
VELOCITYLIMITER_TRANSACTIONEVENT_CONSUMER_TOPIC=transactions-event-v1 \
VELOCITYLIMITER_TRANSACTIONEVENT_PRODUCER_TOPIC=transactions-event-v1 \
VELOCITYLIMITER_TRANSACTIONEVALEVENT_PRODUCER_TOPIC=transaction-evaluated-event-v1 \
VELOCITYLIMITER_TRANSACTIONEVALEVENT_CONSUMER_TOPIC=transaction-evaluated-event-v1 \
VELOCITYLIMITER_EXPECTEDFILEPATH=test/testdata/transactions_output_expected.txt \
VELOCITYLIMITER_OUTPUTDIRPATH=out/ \
go run "$1"