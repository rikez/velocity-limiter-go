#!/bin/bash

# Publish the image

# Check required commands
command -v docker >/dev/null 2>&1 || { echo 'Please install docker or use image that has it'; exit 1; }


paths=$(find "dockerfiles" -maxdepth 1 -mindepth 1 -name "*")
if [[ $(echo "$paths" | wc -l) -eq 0 ]]; then
    echo "No dockerfiles found"
    exit 1
fi

branch="$(git rev-parse --abbrev-ref HEAD)"
commit="git rev-parse --short=9 HEAD"
now="$(date -u '+%Y%m%d')"
tag="$branch-$now"

for path in $paths; do
    name=$(basename "$path")
    image="enrich/$name:$tag"
    image_latest="enrich/$name:latest"

    echo "Pushing to DockerHub: docker push ${image}"
    docker push "$image"
    docker push "$image_latest"
done