# Velocity Limiter

This program accepts or declines attempts to load funds into customers' accounts in real-time based on predefined criteria.

## Decisions

* **`In Memory PubSub`**
    * To simulate a real world application where the transactions are streamed accross topics for different purposes. For instance, in this app there are two topics, the `transaction-event-v1` to handle validation and persistence, and the `transaction-eval-event-v1` to handle the writes to an output, which in this case is a file.
* **`In Memory Database`**
    * To avoid the use of external dependencies;
    * To express knowledge on Data Structures and how to use Golang built-ins.

## Architecture

Project structure

#### **comms**

The layer that deals domain events and act as a "glue" between other layers.

#### **model**

The layer that deals with the domain model.

#### **service**

The layer that deals with pure business logic.

#### **repository**

The layer that deals with database access.

#### **task**

Package to place all pieces of work to be done. In this project, there is a task to process the input file.

#### **mapper**
 
Package responsible for mapping raw data to the actual model

#### **transactionlimiter**

Package to validate if a transaction is under the expected limits to be accepted.

#### **pubsub**

An in memory implementation of the Publish-Subscribe pattern

### **Diagrams**

**Process input file**

```mermaid
sequenceDiagram
    participant TP as TransactionProcessor
    participant TM as TransactionMapper
    participant PS as PubSub
    loop Every line in file
        TP->>TM: Validate content integrity and map raw transaction
        TP->>PS: Publish mapped transaction to `transaction-event-v1` topic
    end
```

**Handles transaction validation and creation**

```mermaid
sequenceDiagram
    participant PS as PubSub
    participant TEH as TransactionEventHandler
    participant SVC as Service
    participant DB as DB
    participant TEP as TransactionEvalEventPublisher
    PS->>TEH: Received new transaction event
    TEH->>+SVC: Create transaction
    SVC->>+DB: Validate and create transaction
    DB-->>-SVC: Op result
    SVC->>TEP: Publish transaction to `transaction-eval-event-v1` topic
    SVC-->-TEH: Op result
```

**Generates an output**

```mermaid
sequenceDiagram
    participant PS as PubSub
    participant TEH as TransactionEvalEventHandler
    participant WS as WritableStream
    PS->>TEH: Received new transaction eval event
    TEH->>WS: Writes (appends) new line to a writable stream
```

## References

* [Project structure](https://github.com/golang-standards/project-layout)
* [Mermaid markdown diagrams](http://mermaid-js.github.io/mermaid/)

## Improvements

* Store the loads in a transactional database, like MySQL or PostgreSQL;
* Add a cache layer, like Redis or Memcached, to allow fast access to validate incoming transactions;
* Add a cache eviction policy to eliminate previously validated transactions (from previous days and weeks);
* Use a messaging system to scale and increase performance of real-time transactions.
    * [Could be Kafka due to its high throughput and capabilities to ensure the delivery of ordered messages](https://www.instaclustr.com/a-visual-understanding-to-ensuring-your-kafka-data-is-literally-in-order/).