module gitlab.com/rikez/velocity-limiter-go.git

go 1.14

require (
	github.com/fortytw2/leaktest v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
)
