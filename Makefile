.PHONY: help check test lint compile build publish generate install-tools install-deps fix-tidy start

help: ## Show this help
	@echo "Help"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "    \033[36m%-20s\033[93m %s\n", $$1, $$2}'

### Code validation
check: ## Run all checks: test and lint
	@bash scripts/check.sh

test: ## Run tests for all go packages
	@bash scripts/checks/test.sh

coverage: test ## Run tests and open coverage report
	@go tool cover -html=.test_coverage.txt

lint: ## Run lint on the codebase, printing any style errors
	@bash scripts/checks/lint.sh

fix-tidy: ## Fix go.mod inconsistency
	@bash scripts/local/fix-tidy.sh

compile: ## Compile the binary
	@bash scripts/compile.sh

build: ## Build the image
	@bash scripts/build.sh

publish: ## Publish the image
	@bash scripts/publish.sh

generate: ## Generates read all generates references and executes them 
	@go generate ./...

install-tools: ## Install external tools
	@bash scripts/install-tools.sh

install-deps: ## Prefetch deps to ensure required versions are downloaded
	@GO111MODULE=on go mod tidy
	@GO111MODULE=on go mod verify
	@GO111MODULE=on go mod vendor

start: ## Exec project locally
	@bash scripts/local/start.sh $(PWD)/cmd/velocity-limiter