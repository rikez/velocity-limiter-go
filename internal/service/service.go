package service

import (
	"errors"
	"strconv"

	"gitlab.com/rikez/velocity-limiter-go.git/internal/comms/transactionevalevent"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/model"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/repository"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/transactionlimiter"
)

//go:generate mockery --case underscore --output mocks --outpkg servicemock --name Service

// Service defines a contract to perform actions related to the customer transactions
type Service interface {
	// CreateTransaction creates a new transaction and emit event to the interested parties
	CreateTransaction(*model.Transaction) error
}

type defaultService struct {
	publisher          *transactionevalevent.Producer
	transactionLimiter transactionlimiter.InMemoryLimiter
	inMemoryRepository repository.InMemoryRepository
}

// NewService creates anew instance fo service
func NewService(
	publisher *transactionevalevent.Producer,
	transactionLimiter transactionlimiter.InMemoryLimiter,
	inMemoryRepository repository.InMemoryRepository,
) Service {
	return &defaultService{
		publisher:          publisher,
		transactionLimiter: transactionLimiter,
		inMemoryRepository: inMemoryRepository,
	}
}

func (s *defaultService) CreateTransaction(t *model.Transaction) (err error) {
	exists := s.verifyTransactionExists(t.ID, t.CustomerID)
	if exists {
		return
	}
	if err = s.inMemoryRepository.Register(*t); err != nil {
		return
	}

	transactionEval := model.TransactionEvaluation{
		ID:         strconv.Itoa(t.ID),
		CustomerID: strconv.Itoa(t.CustomerID),
		Accepted:   false,
	}

	defer func() {
		err = s.publisher.Send(&transactionEval)
	}()

	metadata, metadataErr := s.getCustomerMetadata(t)
	if metadataErr != nil && errors.Is(metadataErr, repository.ErrCorruptedCustomerStorage) {
		return
	}

	if err = s.transactionLimiter.CanLoadAmount(metadata); err != nil {
		return
	}
	if err = s.inMemoryRepository.Create(*t); err != nil {
		return
	}

	transactionEval.Accepted = true

	return err
}

func (s *defaultService) verifyTransactionExists(id, customerID int) bool {
	_, err := s.inMemoryRepository.FindTransaction(id, customerID)
	return err == nil
}

func (s *defaultService) getCustomerMetadata(t *model.Transaction) (transactionlimiter.CustomerMetadata, error) {
	var metadata transactionlimiter.CustomerMetadata
	metadata.Amount = t.Amount

	dt, err := s.inMemoryRepository.GetCustomerDailyTransactions(t.CustomerID, t.Timestamp)
	if err != nil {
		return metadata, err
	}

	wt, err := s.inMemoryRepository.GetCustomerWeeklyTransactions(t.CustomerID, t.Timestamp)
	if err != nil {
		return metadata, err
	}

	if wt != nil && dt != nil {
		metadata.LoadedAmountWeek = wt.TotalAmountLoaded
		metadata.LoadedAmountToday = dt.TotalAmountLoaded
		metadata.LoadsToday = dt.Loads
	}

	return metadata, nil
}
