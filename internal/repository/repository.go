package repository

import (
	"time"

	"gitlab.com/rikez/velocity-limiter-go.git/internal/model"
)

//go:generate mockery --case underscore --output mocks --outpkg repositorymock --name InMemoryRepository

// InMemoryRepository defines the contract for the Transactions repository in memory
type InMemoryRepository interface {
	// FindTransaction returns a transaction by id and customer id
	FindTransaction(id, customerID int) (*model.Transaction, error)
	// GetCustomerDailyTransactions returns the transactions made on a given day
	GetCustomerDailyTransactions(customerID int, day time.Time) (*model.DailyTransaction, error)
	// GetCustomerDailyTransactions returns the transactions made on a given week
	GetCustomerWeeklyTransactions(customerID int, week time.Time) (*model.WeeklyTransaction, error)
	// Register registers a new transaction even if it is NOT valid
	Register(t model.Transaction) error
	// Create creates a new transaction
	Create(t model.Transaction) error
}
