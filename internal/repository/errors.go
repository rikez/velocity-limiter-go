package repository

import "errors"

var (
	// ErrCorruptedCustomerStorage returned when customer storage is corrupted (not supposed to happen...)
	ErrCorruptedCustomerStorage = errors.New("customer database storage is corrupted")
	// ErrDuplicatedTransaction returned when a transaction already exists
	ErrDuplicatedTransaction = errors.New("already exists")
	// ErrNotFound returned when a transaction already exists
	ErrNotFound = errors.New("not found")
)
