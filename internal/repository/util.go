package repository

import (
	"fmt"
	"time"
)

// GetISOWeekIndexFromTimestamp returns an YYYY_Week index from the transaction timestamp
func GetISOWeekIndexFromTimestamp(ts time.Time) string {
	y, w := ts.ISOWeek()
	return fmt.Sprintf("%d_%d", y, w)
}

// GetCurrentDateIndexFromTimestamp returns an YYYY_MM_DD index from the transaction timestamp
func GetCurrentDateIndexFromTimestamp(ts time.Time) string {
	y, m, d := ts.Date()
	return fmt.Sprintf("%d_%d_%d", y, m, d)
}
