package repository

import (
	"errors"
	"sync"
	"time"

	"gitlab.com/rikez/velocity-limiter-go.git/internal/model"
)

/**
 * map[customer_id] {
 *   weekly: map[YYYY_Week] -> { TotalAmountLoaded float64 }
 *   daily: map[YYYY-MM-DD] -> { TotalAmountLoaded float64, Loads int }
 *   acceptedTransactions: map[transaction_id] -> {}
 *   allTransactions: map[transaction_id] -> {}
 * }
 */

// Using sync.Map because it is thread safe

type storage struct {
	customer *sync.Map
}

type indexes struct {
	weekly               *sync.Map
	daily                *sync.Map
	acceptedTransactions *sync.Map
	allTransactions      *sync.Map
}
type inMemoryRepository struct {
	m *storage
}

// NewInMemory creates a new instance of Repository
func NewInMemory() InMemoryRepository {
	return &inMemoryRepository{
		m: &storage{
			customer: &sync.Map{},
		},
	}
}

func (r *inMemoryRepository) FindTransaction(id, customerID int) (*model.Transaction, error) {
	err := r.checkTransactionExists(id, customerID)
	if err != nil {
		return nil, err
	}

	return &model.Transaction{ID: id, CustomerID: customerID}, nil
}

func (r *inMemoryRepository) GetCustomerDailyTransactions(customerID int, day time.Time) (*model.DailyTransaction, error) {
	customer, err := r.castCustomerMap(customerID)
	if err != nil {
		return nil, err
	}

	todayKey := GetCurrentDateIndexFromTimestamp(day)
	return r.castDailyMapAtKey(customer.daily, todayKey)
}

func (r *inMemoryRepository) GetCustomerWeeklyTransactions(customerID int, week time.Time) (*model.WeeklyTransaction, error) {
	customer, err := r.castCustomerMap(customerID)
	if err != nil {
		return nil, err
	}

	weekKey := GetISOWeekIndexFromTimestamp(week)
	return r.castWeeklyMapAtKey(customer.weekly, weekKey)
}

func (r *inMemoryRepository) Register(t model.Transaction) error {
	customer, err := r.getCustomerIndexes(t)
	if err != nil {
		return err
	}

	return r.createTransaction(customer.allTransactions, t)
}

func (r *inMemoryRepository) Create(t model.Transaction) error {
	customer, err := r.getCustomerIndexes(t)
	if err != nil {
		return err
	}

	if err := r.createTransaction(customer.acceptedTransactions, t); err != nil {
		return err
	}
	if err := r.createDailyTransactionIndex(customer.daily, t); err != nil {
		return err
	}
	return r.createWeeklyTransactionIndex(customer.weekly, t)
}

// Helpers

func (r *inMemoryRepository) getCustomerIndexes(t model.Transaction) (*indexes, error) {
	var customer *indexes

	var castErr error
	customer, castErr = r.castCustomerMap(t.CustomerID)
	if castErr != nil {
		if !errors.Is(castErr, ErrNotFound) {
			return nil, castErr
		}

		customer = &indexes{
			weekly:               &sync.Map{},
			daily:                &sync.Map{},
			acceptedTransactions: &sync.Map{},
			allTransactions:      &sync.Map{},
		}

		r.m.customer.Store(t.CustomerID, customer)
	}

	return customer, nil
}

func (r *inMemoryRepository) createDailyTransactionIndex(dailyMap *sync.Map, t model.Transaction) error {
	todayKey := GetCurrentDateIndexFromTimestamp(t.Timestamp)

	dt, err := r.castDailyMapAtKey(dailyMap, todayKey)
	if err != nil {
		if !errors.Is(err, ErrNotFound) {
			return err
		}

		dt := &model.DailyTransaction{TotalAmountLoaded: t.Amount, Loads: 1}
		dailyMap.Store(todayKey, dt)
		return nil
	}

	dt.Loads++
	dt.TotalAmountLoaded += t.Amount
	dailyMap.Store(todayKey, dt)
	return nil
}

func (r *inMemoryRepository) createWeeklyTransactionIndex(weeklyMap *sync.Map, t model.Transaction) error {
	weekKey := GetISOWeekIndexFromTimestamp(t.Timestamp)

	wt, err := r.castWeeklyMapAtKey(weeklyMap, weekKey)
	if err != nil {
		if !errors.Is(err, ErrNotFound) {
			return err
		}

		wt := &model.WeeklyTransaction{TotalAmountLoaded: t.Amount}
		weeklyMap.Store(weekKey, wt)
		return nil
	}

	wt.TotalAmountLoaded += t.Amount
	weeklyMap.Store(weekKey, wt)
	return nil
}

func (r *inMemoryRepository) createTransaction(transactionsMap *sync.Map, t model.Transaction) error {
	if transactionsMap != nil {
		_, found := transactionsMap.Load(t.ID)
		if found {
			return ErrDuplicatedTransaction
		}
	} else {
		transactionsMap = &sync.Map{}
	}

	transactionsMap.Store(t.ID, struct{}{})

	return nil
}

func (r *inMemoryRepository) castDailyMapAtKey(m *sync.Map, key string) (*model.DailyTransaction, error) {
	if m != nil {
		i, found := m.Load(key)
		if found {
			dailyTransaction, castOk := i.(*model.DailyTransaction)
			if !castOk {
				return nil, ErrCorruptedCustomerStorage
			}

			return dailyTransaction, nil
		}
	}

	return nil, ErrNotFound
}

func (r *inMemoryRepository) castWeeklyMapAtKey(m *sync.Map, key string) (*model.WeeklyTransaction, error) {
	if m != nil {
		i, found := m.Load(key)
		if found {
			weeklyTransaction, castOk := i.(*model.WeeklyTransaction)
			if !castOk {
				return nil, ErrCorruptedCustomerStorage
			}

			return weeklyTransaction, nil
		}
	}

	return nil, ErrNotFound
}

func (r *inMemoryRepository) castCustomerMap(customerID int) (*indexes, error) {
	i, found := r.m.customer.Load(customerID)
	if !found {
		return nil, ErrNotFound
	}

	customer, castOk := i.(*indexes)
	if !castOk {
		return nil, ErrCorruptedCustomerStorage
	}

	return customer, nil
}

func (r *inMemoryRepository) checkTransactionExists(id, customerID int) error {
	customer, err := r.castCustomerMap(customerID)
	if err != nil {
		return err
	}

	if customer.allTransactions == nil {
		return ErrNotFound
	}

	_, ok := customer.allTransactions.Load(id)
	if !ok {
		return ErrNotFound
	}

	return nil
}
