package repository

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/model"
)

// TODO: maybe reimplement the tests with table driven approach

func TestInMemoryRepository(t *testing.T) {
	t.Parallel()

	t.Run("should create new transaction", func(t *testing.T) {
		repo := NewInMemory()

		t1 := model.Transaction{
			ID:         123,
			CustomerID: 1234,
			Amount:     5000.0,
			Timestamp:  time.Now(),
		}

		err := repo.Register(t1)
		assert.NoError(t, err)

		err = repo.Create(t1)
		assert.NoError(t, err)

		res, err := repo.FindTransaction(t1.ID, t1.CustomerID)
		assert.NoError(t, err)
		assert.NotNil(t, res)

		assert.Equal(t, t1.ID, res.ID)
		assert.Equal(t, t1.CustomerID, res.CustomerID)
	})

	t.Run("should return duplicated transaction when it already exists", func(t *testing.T) {
		repo := NewInMemory()

		t1 := model.Transaction{
			ID:         123,
			CustomerID: 1234,
			Amount:     5000.0,
			Timestamp:  time.Now(),
		}

		err := repo.Create(t1)
		assert.NoError(t, err)

		err = repo.Create(t1)
		assert.Error(t, err)
		assert.True(t, errors.Is(err, ErrDuplicatedTransaction))
	})

	t.Run("should return customer report for the provided day and week", func(t *testing.T) {
		repo := NewInMemory()

		loads := 10
		amount := 500.0
		customerID := 123

		for i := 1; i <= loads; i++ {
			transaction := model.Transaction{
				ID:         i,
				CustomerID: customerID,
				Amount:     amount,
				Timestamp:  time.Now(),
			}
			err := repo.Create(transaction)
			assert.NoError(t, err)
		}

		// Queries

		dt, err := repo.GetCustomerDailyTransactions(customerID, time.Now())
		assert.NoError(t, err)
		assert.Equal(t, loads, dt.Loads)
		assert.Equal(t, amount*float64(loads), dt.TotalAmountLoaded)

		wt, err := repo.GetCustomerWeeklyTransactions(customerID, time.Now())
		assert.NoError(t, err)
		assert.Equal(t, amount*float64(loads), wt.TotalAmountLoaded)
	})

	t.Run("should return customer report for the provided day and week", func(t *testing.T) {
		repo := NewInMemory()

		amount := 500.0
		customerID := 123

		timeWeek1 := time.Now()
		timeWeek2 := time.Now().Add(time.Hour * 24 * 10)

		transactionWeek1 := model.Transaction{
			ID:         1,
			CustomerID: customerID,
			Amount:     amount,
			Timestamp:  timeWeek1,
		}
		err := repo.Create(transactionWeek1)
		assert.NoError(t, err)

		transactionWeek2 := model.Transaction{
			ID:         2,
			CustomerID: customerID,
			Amount:     amount,
			Timestamp:  timeWeek2,
		}
		err = repo.Create(transactionWeek2)
		assert.NoError(t, err)

		// Queries

		dt, err := repo.GetCustomerDailyTransactions(customerID, timeWeek1)
		assert.NoError(t, err)
		assert.Equal(t, 1, dt.Loads)
		assert.Equal(t, amount*1, dt.TotalAmountLoaded)

		dt, err = repo.GetCustomerDailyTransactions(customerID, timeWeek2)
		assert.NoError(t, err)
		assert.Equal(t, 1, dt.Loads)
		assert.Equal(t, amount*1, dt.TotalAmountLoaded)

		// Week 1
		wt, err := repo.GetCustomerWeeklyTransactions(customerID, timeWeek1)
		assert.NoError(t, err)
		assert.Equal(t, amount, wt.TotalAmountLoaded)

		// Week 2
		wt, err = repo.GetCustomerWeeklyTransactions(customerID, timeWeek2)
		assert.NoError(t, err)
		assert.Equal(t, amount, wt.TotalAmountLoaded)
	})
}
