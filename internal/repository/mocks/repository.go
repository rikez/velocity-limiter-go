// Code generated by mockery v1.0.0. DO NOT EDIT.

package repositorymock

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	model "gitlab.com/rikez/velocity-limiter-go.git/internal/model"
)

// Repository is an autogenerated mock type for the Repository type
type Repository struct {
	mock.Mock
}

// Create provides a mock function with given fields: _a0, _a1
func (_m *Repository) Create(_a0 context.Context, _a1 model.Transaction) error {
	ret := _m.Called(_a0, _a1)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, model.Transaction) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetByCustomerID provides a mock function with given fields: ctx, id
func (_m *Repository) GetByCustomerID(ctx context.Context, id string) ([]model.Transaction, error) {
	ret := _m.Called(ctx, id)

	var r0 []model.Transaction
	if rf, ok := ret.Get(0).(func(context.Context, string) []model.Transaction); ok {
		r0 = rf(ctx, id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]model.Transaction)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByTransactionID provides a mock function with given fields: ctx, id
func (_m *Repository) GetByTransactionID(ctx context.Context, id string) (*model.Transaction, error) {
	ret := _m.Called(ctx, id)

	var r0 *model.Transaction
	if rf, ok := ret.Get(0).(func(context.Context, string) *model.Transaction); ok {
		r0 = rf(ctx, id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*model.Transaction)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
