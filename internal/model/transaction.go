package model

import (
	"time"
)

// Transaction stands for a transaction made to a customer account
type Transaction struct {
	ID         int
	CustomerID int
	Amount     float64
	Timestamp  time.Time
}

// TransactionEvaluation stands for a transaction response after attempt to create
type TransactionEvaluation struct {
	ID         string `json:"id"`
	CustomerID string `json:"customer_id"`
	Accepted   bool   `json:"accepted"`
}

// DailyTransaction represents the transactions made a customer in a given day of the year
type DailyTransaction struct {
	TotalAmountLoaded float64
	Loads             int
}

// WeeklyTransaction represents the transactions made a customer in a given week of the year
type WeeklyTransaction struct {
	TotalAmountLoaded float64
}
