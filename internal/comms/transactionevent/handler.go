package transactionevent

import (
	"context"

	"github.com/sirupsen/logrus"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/model"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/service"
	"gitlab.com/rikez/velocity-limiter-go.git/pkg/pubsub"
)

const transactionEventHandlerName = "transaction_event"

type defaultHandler struct {
	transactionSvc service.Service
}

// NewHandler creates a new instance of transaction event handler
func NewHandler(
	transactionSvc service.Service,
) pubsub.Handler {
	return &defaultHandler{
		transactionSvc: transactionSvc,
	}
}

func (h *defaultHandler) Name() string {
	return transactionEventHandlerName
}

func (h *defaultHandler) Handle(ctx context.Context, ev pubsub.Event) {
	h.handle(ctx, ev.Data)
	ev.Ack()
}

func (h *defaultHandler) handle(ctx context.Context, data interface{}) {
	t, ok := data.(*model.Transaction)
	if !ok {
		logrus.Errorf("Unexpected transaction event data: %+v", data)
		return
	}

	logEntry := logrus.WithContext(ctx).WithFields(logrus.Fields{
		"id":  t.ID,
		"cid": t.CustomerID,
		"cad": t.Amount,
		"ts":  t.Timestamp.String(),
	})

	logEntry.Debug("Received TransactionEvent")

	if err := h.transactionSvc.CreateTransaction(t); err != nil {
		logEntry.Debugf("Failed to create transaction: %+v", err)
	}
}
