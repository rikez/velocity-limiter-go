package comms

// ConsumerConfig defines settings to create a new event subscription
type ConsumerConfig struct {
	Topic string `required:"true"`
}

// ProducerConfig defines settings to create a new publisher
type ProducerConfig struct {
	Topic string `required:"true"`
}
