package transactionevalevent

import (
	"gitlab.com/rikez/velocity-limiter-go.git/internal/comms"
	"gitlab.com/rikez/velocity-limiter-go.git/pkg/pubsub"
)

// Consumer wraps and handle the pubsub subscriber to start receiving events
type Consumer struct {
	cfg     comms.ConsumerConfig
	sub     pubsub.Subscriber
	handler pubsub.Handler
}

// NewConsumer creates a new instance of consumer
func NewConsumer(cfg comms.ConsumerConfig, sub pubsub.Subscriber, handler pubsub.Handler) *Consumer {
	return &Consumer{cfg: cfg, sub: sub, handler: handler}
}

// Consume register a new subscription with a handler
func (c *Consumer) Consume() {
	c.sub.Subscribe(c.cfg.Topic, c.handler)
}
