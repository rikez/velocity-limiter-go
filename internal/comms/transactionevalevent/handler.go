package transactionevalevent

import (
	"context"
	"encoding/json"
	"fmt"
	"io"

	"github.com/sirupsen/logrus"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/model"
	"gitlab.com/rikez/velocity-limiter-go.git/pkg/pubsub"
)

const transactionEvalEventHandlerName = "transaction_eval_event"

type defaultHandler struct {
	w io.Writer
}

// NewHandler creates a new instance of transaction event handler
func NewHandler(w io.Writer) pubsub.Handler {
	return &defaultHandler{w}
}

func (h *defaultHandler) Name() string {
	return transactionEvalEventHandlerName
}

func (h *defaultHandler) Handle(ctx context.Context, ev pubsub.Event) {
	h.handle(ctx, ev.Data)
	ev.Ack()
}

func (h *defaultHandler) handle(ctx context.Context, data interface{}) {
	transactionEval, ok := data.(*model.TransactionEvaluation)
	if !ok {
		logrus.Errorf("Unexpected transaction eval event data: %+v", data)
		return
	}

	logEntry := logrus.WithContext(ctx).WithFields(logrus.Fields{
		"id":  transactionEval.ID,
		"cid": transactionEval.CustomerID,
		"acc": transactionEval.Accepted,
	})

	logEntry.Debug("Received TransactionEvalEvent")

	b, err := json.Marshal(transactionEval)
	if err != nil {
		logEntry.Debug(err)
		return
	}

	_, err = fmt.Fprintln(h.w, string(b))
	if err != nil {
		logEntry.Debug(err)
		return
	}
}
