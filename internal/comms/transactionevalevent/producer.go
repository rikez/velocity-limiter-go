package transactionevalevent

import (
	"gitlab.com/rikez/velocity-limiter-go.git/internal/comms"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/model"
	"gitlab.com/rikez/velocity-limiter-go.git/pkg/pubsub"
)

// Producer wraps and handle the pubsub publisher to start sending events
type Producer struct {
	cfg comms.ProducerConfig
	pub pubsub.Publisher
}

// NewProducer creates a new instance of consumer
func NewProducer(cfg comms.ProducerConfig, pub pubsub.Publisher) *Producer {
	return &Producer{cfg: cfg, pub: pub}
}

// Send publishes a new model.TransactionEvaluation message to the TransactionEvalEvent topic
func (c *Producer) Send(msg *model.TransactionEvaluation) error {
	return c.pub.Publish(c.cfg.Topic, msg)
}
