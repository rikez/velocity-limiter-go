package mapper

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLimiterErrors(t *testing.T) {
	t.Parallel()

	samples := []struct {
		in       error
		expected string
	}{
		{
			in:       newMapperErr(errors.New("failed to convert string to int"), "id", "123"),
			expected: "failed to map id(123): failed to convert string to int",
		},
	}

	for i, tt := range samples {
		t.Run(fmt.Sprintf("%d: testing errors", i), func(t *testing.T) {
			assert.EqualError(t, tt.in, tt.expected)
		})
	}
}
