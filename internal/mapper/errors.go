package mapper

import "fmt"

// BaseErr mapping errors
type BaseErr struct {
	Err error
}

// newMapperErr creates a new mapping error
func newMapperErr(err error, field string, actual interface{}) BaseErr {
	return BaseErr{
		Err: fmt.Errorf("failed to map %s(%+v): %w", field, actual, err),
	}
}

func (e BaseErr) Error() string {
	return e.Err.Error()
}
