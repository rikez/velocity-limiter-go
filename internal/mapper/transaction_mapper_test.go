package mapper

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/model"
)

func TestMapRawContentToTransaction(t *testing.T) {
	t.Parallel()

	type output struct {
		errored     bool
		transaction *model.Transaction
	}

	samples := []struct {
		in       RawTransaction
		expected output
	}{
		{
			in:       RawTransaction{ID: "AAA", CustomerID: "BBB", LoadAmount: "$500.0", Time: "2000-01-01T14:19:08Z"},
			expected: output{errored: true, transaction: nil},
		},
		{
			in:       RawTransaction{ID: "123", CustomerID: "BBB", LoadAmount: "$500.0", Time: "2000-01-01T14:19:08Z"},
			expected: output{errored: true, transaction: nil},
		},
		{
			in:       RawTransaction{ID: "123", CustomerID: "1234", LoadAmount: "R$500.0", Time: "2000-01-01T14:19:08Z"},
			expected: output{errored: true, transaction: nil},
		},
		{
			in:       RawTransaction{ID: "123", CustomerID: "1234", LoadAmount: "$500.0", Time: "invalid date"},
			expected: output{errored: true, transaction: nil},
		},
		{
			in: RawTransaction{ID: "123", CustomerID: "1234", LoadAmount: "$500.0", Time: "2000-01-01T14:19:08Z"},
			expected: output{errored: false, transaction: &model.Transaction{
				ID:         123,
				CustomerID: 1234,
				Amount:     500.0,
				Timestamp:  time.Date(int(2000), time.January, int(1), int(14), int(19), int(8), int(0), time.UTC),
			}},
		},
	}

	for i, tt := range samples {
		testName := fmt.Sprintf("%d: should mapping raw content to transaction", i)

		t.Run(testName, func(t *testing.T) {
			transaction, err := MapRawContentToTransaction(tt.in)

			if tt.expected.errored {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}

			assert.Equal(t, tt.expected.transaction, transaction)
		})
	}
}
