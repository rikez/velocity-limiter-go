package mapper

import (
	"strconv"
	"strings"
	"time"

	"gitlab.com/rikez/velocity-limiter-go.git/internal/model"
)

// RawTransaction representation of the transactions in the input files
type RawTransaction struct {
	ID         string `json:"id"`
	CustomerID string `json:"customer_id"`
	LoadAmount string `json:"load_amount"`
	Time       string `json:"time"`
}

// MapRawContentToTransaction maps RawTransaction to model.Transaction
func MapRawContentToTransaction(rt RawTransaction) (*model.Transaction, error) {
	transactionIDAsInt, err := strconv.Atoi(rt.ID)
	if err != nil {
		return nil, newMapperErr(err, "id", rt.ID)
	}

	customerIDAsInt, err := strconv.Atoi(rt.CustomerID)
	if err != nil {
		return nil, newMapperErr(err, "customer_id", rt.CustomerID)
	}

	loadAmount := strings.Replace(rt.LoadAmount, "$", "", 1)
	loadAmountAsFloat, err := strconv.ParseFloat(loadAmount, 64)
	if err != nil {
		return nil, newMapperErr(err, "load_amount", rt.LoadAmount)
	}

	utcTime, err := time.Parse(time.RFC3339, rt.Time)
	if err != nil {
		return nil, newMapperErr(err, "time", rt.Time)
	}

	return &model.Transaction{
		ID:         transactionIDAsInt,
		CustomerID: customerIDAsInt,
		Amount:     loadAmountAsFloat,
		Timestamp:  utcTime,
	}, nil
}
