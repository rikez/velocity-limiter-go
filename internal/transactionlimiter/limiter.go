package transactionlimiter

//go:generate mockery --case underscore --output mocks --outpkg limitermock --name InMemoryLimiter

// InMemoryLimiter defines the contract for the Transactions in memory limiter
type InMemoryLimiter interface {
	// CanLoadAmount checks if a transaction is accepted or declined
	CanLoadAmount(CustomerMetadata) error
}

// CustomerMetadata stands for the information necessary to check the enforced limits
type CustomerMetadata struct {
	Amount            float64
	LoadedAmountToday float64
	LoadedAmountWeek  float64
	LoadsToday        int
}
