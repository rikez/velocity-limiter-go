package transactionlimiter

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCanLoadAmount(t *testing.T) {
	t.Parallel()

	cfg := InMemoryConfig{
		MaxAmountPerDay:  5_000,
		MaxAmountPerWeek: 20_000,
		MaxLoadsPerDay:   3,
	}

	samples := []struct {
		metadata CustomerMetadata
		expected error
	}{
		{
			metadata: CustomerMetadata{Amount: 500.0, LoadedAmountToday: 0, LoadedAmountWeek: 4000.0, LoadsToday: 0},
			expected: nil,
		},
		{
			metadata: CustomerMetadata{Amount: 5000.0, LoadedAmountToday: 0, LoadedAmountWeek: 18000.0, LoadsToday: 0},
			expected: ErrReachedMaxLoadAmountPerWeek,
		},
		{
			metadata: CustomerMetadata{Amount: 5000.0, LoadedAmountToday: 4000.0, LoadedAmountWeek: 18000.0, LoadsToday: 0},
			expected: ErrReachedMaxLoadAmountPerDay,
		},
		{
			metadata: CustomerMetadata{Amount: 500.0, LoadedAmountToday: 4000.0, LoadedAmountWeek: 10000.0, LoadsToday: 3},
			expected: ErrReachedMaxLoadsPerDay,
		},
	}

	inMemoryLimiter := NewInMemory(cfg)

	for i, tt := range samples {
		testName := fmt.Sprintf("%d: loading amount %f", i, tt.metadata.Amount)

		t.Run(testName, func(t *testing.T) {
			err := inMemoryLimiter.CanLoadAmount(tt.metadata)
			assert.Equal(t, tt.expected, err)
		})
	}
}
