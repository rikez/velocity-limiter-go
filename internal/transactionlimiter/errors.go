package transactionlimiter

import "errors"

var (
	// ErrReachedMaxLoadsPerDay thrown when customer reaches max loads per day
	ErrReachedMaxLoadsPerDay = errors.New("customer reached max loads per day")
	// ErrReachedMaxLoadAmountPerDay thrown when customer reaches max load amount per day
	ErrReachedMaxLoadAmountPerDay = errors.New("customer reached max load amount per day")
	// ErrReachedMaxLoadAmountPerWeek thrown when customer reaches max load amount per week
	ErrReachedMaxLoadAmountPerWeek = errors.New("customer reached max load amount per week")
)

// IsLimiterErr verfies if err cause is due to the limiter rules
func IsLimiterErr(err error) bool {
	return (errors.Is(err, ErrReachedMaxLoadsPerDay) ||
		errors.Is(err, ErrReachedMaxLoadAmountPerDay) ||
		errors.Is(err, ErrReachedMaxLoadAmountPerWeek))
}
