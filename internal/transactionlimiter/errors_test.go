package transactionlimiter

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLimiterErrors(t *testing.T) {
	t.Parallel()

	samples := []struct {
		in       error
		expected bool
	}{
		{
			in:       ErrReachedMaxLoadAmountPerDay,
			expected: true,
		},
		{
			in:       ErrReachedMaxLoadAmountPerWeek,
			expected: true,
		},
		{
			in:       ErrReachedMaxLoadsPerDay,
			expected: true,
		},
		{
			in:       errors.New("IO error: failed to fetch rules"),
			expected: false,
		},
	}

	for i, tt := range samples {
		t.Run(fmt.Sprintf("%d: testing errors", i), func(t *testing.T) {
			assert.Equal(t, tt.expected, IsLimiterErr(tt.in))
		})
	}
}
