package transactionlimiter

// InMemoryConfig establishes settings to limit transaction by customer for in memory limiter
type InMemoryConfig struct {
	MaxAmountPerDay  float64 `default:"5000.0"`
	MaxAmountPerWeek float64 `default:"20000.0"`
	MaxLoadsPerDay   int     `default:"3"`
}

type defaultInMemoryLimiter struct {
	cfg InMemoryConfig
}

// NewInMemory creates an instance of an in memory limiter
func NewInMemory(cfg InMemoryConfig) InMemoryLimiter {
	return &defaultInMemoryLimiter{cfg: cfg}
}

func (m *defaultInMemoryLimiter) CanLoadAmount(metadata CustomerMetadata) error {
	if metadata.LoadsToday+1 > m.cfg.MaxLoadsPerDay {
		return ErrReachedMaxLoadsPerDay
	}
	if (metadata.Amount + metadata.LoadedAmountToday) > m.cfg.MaxAmountPerDay {
		return ErrReachedMaxLoadAmountPerDay
	}
	if (metadata.Amount + metadata.LoadedAmountWeek) > m.cfg.MaxAmountPerWeek {
		return ErrReachedMaxLoadAmountPerWeek
	}

	return nil
}
