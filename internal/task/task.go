package task

//go:generate mockery --case underscore --output mocks --outpkg taskmock --name Task

// Task defines a contract for implement long-running tasks
type Task interface {
	Start() error
	Stop() error
	Fields() map[string]interface{}
}
