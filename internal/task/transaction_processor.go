package task

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/comms/transactionevent"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/mapper"
	"gitlab.com/rikez/velocity-limiter-go.git/internal/model"
)

const taskName = "transaction_processor"

// TransactionProcessorConfig defines settings for the transaction processor task
type TransactionProcessorConfig struct {
	Path string `required:"true"`
}

type transactionProcessor struct {
	cfg       TransactionProcessorConfig
	publisher *transactionevent.Producer
}

// NewTransactionProcessorTask creates a new instance of transactionProcessor
func NewTransactionProcessorTask(cfg TransactionProcessorConfig, publisher *transactionevent.Producer) (Task, error) {
	_, err := os.Stat(cfg.Path)
	if err != nil {
		return nil, fmt.Errorf("failed to check file existence: %v", err)
	}

	return &transactionProcessor{cfg: cfg, publisher: publisher}, nil
}

func (s *transactionProcessor) Start() error {
	f, err := os.Open(s.cfg.Path)
	if err != nil {
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	logEntry := logrus.WithFields(s.Fields())

	logEntry.Infof("Scanning transactions from %s...", s.cfg.Path)

	counter := 0
	t0 := time.Now()

	// Executing one by one to ensure chronological order
	for scanner.Scan() {
		counter++

		jsonLine := scanner.Bytes()

		t, err := s.parseTransaction(jsonLine)
		if err != nil {
			logEntry.Errorf("Failed to parse the transaction[%s]: %v", string(jsonLine), err)
			continue
		}

		if err := s.publisher.Send(t); err != nil {
			return fmt.Errorf("failed to publish the transaction: %v", err)
		}
	}

	if err := scanner.Err(); err != nil {
		return fmt.Errorf("failed to scan transaction file: %v", err)
	}

	logEntry.Infof("Finished scanning %d transactions in %+v", counter, time.Since(t0))

	return nil
}

func (s *transactionProcessor) Stop() error {
	panic("Not implemented")
}

func (s *transactionProcessor) Fields() map[string]interface{} {
	return logrus.Fields{
		"task-name": taskName,
	}
}

func (s *transactionProcessor) parseTransaction(line []byte) (*model.Transaction, error) {
	var rt mapper.RawTransaction

	if err := json.NewDecoder(bytes.NewReader(line)).Decode(&rt); err != nil {
		return nil, fmt.Errorf("failed to decode json into model.Transaction: %v", err)
	}

	return mapper.MapRawContentToTransaction(rt)
}
