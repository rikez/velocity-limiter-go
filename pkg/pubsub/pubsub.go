package pubsub

import (
	"context"
	"fmt"
	"sync"
	"time"
)

// TODO: create unsubscribe method

//go:generate mockery --case underscore --output mocks --outpkg pubsubmock --name Handler
//go:generate mockery --case underscore --output mocks --outpkg pubsubmock --name Publisher
//go:generate mockery --case underscore --output mocks --outpkg pubsubmock --name Subscriber
//go:generate mockery --case underscore --output mocks --outpkg pubsubmock --name PubSub

// Event represents an event within the pubsub context
type Event struct {
	Data interface{}
	Ack  func()
}

// Handler defines a contract to subscribe and handle messages rom topics
type Handler interface {
	// Handle receives an event and acknowledge that when it finishes processing
	Handle(ctx context.Context, ev Event)
	// Name returns the name of the handler
	Name() string
}

// Publisher defines a contract for using publish methods
type Publisher interface {
	// Publish publishes a new message in a given topic
	Publish(topic string, msg interface{}) error
}

// Subscriber defines a contract for using subscribe methods
type Subscriber interface {
	// Subscribe subscribes a handler to a topic to begin receiving events
	Subscribe(topic string, fn Handler)
	// Subscribers returns the number of subscribers a topic has
	Subscribers(topic string) int
}

// PubSub defines a simple contract that any implementation of a pubsub must comply with
type PubSub interface {
	Publisher
	Subscriber
}

type defaultPubSub struct {
	cfg         Config
	subscribers map[string][]Handler
	mtx         sync.RWMutex
}

// NewPubSub creates a new instance of default pubsub
func NewPubSub(cfg Config) PubSub {
	return &defaultPubSub{
		cfg:         cfg,
		subscribers: make(map[string][]Handler),
		mtx:         sync.RWMutex{},
	}
}

func (p *defaultPubSub) Subscribe(topic string, fn Handler) {
	// Using exclusive write mutex to block other write ops
	p.mtx.Lock()
	defer p.mtx.Unlock()

	if v, ok := p.subscribers[topic]; ok {
		p.subscribers[topic] = append(v, fn)
		return
	}

	p.subscribers[topic] = []Handler{fn}
}

func (p *defaultPubSub) Subscribers(topic string) int {
	// Using exclusive write mutex to block other write ops
	p.mtx.RLock()
	defer p.mtx.RUnlock()

	if v, ok := p.subscribers[topic]; ok {
		return len(v)
	}
	return 0
}

func (p *defaultPubSub) Publish(topic string, msg interface{}) error {
	// Using exclusive read mutex to block new subscriptions (writes) until we publish message to all subscribers
	p.mtx.RLock()
	defer p.mtx.RUnlock()

	handlers, ok := p.subscribers[topic]
	if !ok {
		return ErrInvalidTopic
	}

	wg := sync.WaitGroup{}
	errCh := make(chan error)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	for _, h := range handlers {
		h := h
		wg.Add(1)

		// Requiring ack from subcriber to ensure transactions are received and processed
		ackCh := make(chan struct{}, 1)

		ackFn := func() {
			select {
			case <-ctx.Done():
				return
			default:
				ackCh <- struct{}{}
			}
		}
		ev := Event{
			Data: msg,
			Ack:  ackFn,
		}

		go func() {
			defer wg.Done()
			h.Handle(ctx, ev)
		}()

		go func() {
			select {
			case <-time.After(p.cfg.Publisher.Timeout):
				cancel()
				close(ackCh)
				errCh <- fmt.Errorf("failed to publish event to %s: %w", h.Name(), ErrPublishTimeout)
			case <-ackCh:
				return
			}
		}()
	}

	wg.Wait()

	select {
	case err := <-errCh:
		return err
	default:
		return nil
	}
}
