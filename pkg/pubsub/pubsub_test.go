package pubsub

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/fortytw2/leaktest"
	"github.com/stretchr/testify/assert"
)

func TestPubSub(t *testing.T) {
	t.Parallel()

	cfg := Config{
		Publisher: PublisherConfig{
			Timeout: time.Millisecond * 200,
		},
	}

	// Not using table tests for this package because it would add more complexity

	t.Run("happy path", func(t *testing.T) {
		defer leaktest.Check(t)()

		ps := NewPubSub(cfg)

		msg := map[string]string{"name": "Test"}

		handler := testHandler{
			name: "test",
			fn: func(ctx context.Context, ev Event) {
				assert.Equal(t, ev.Data, msg)
				ev.Ack()
			},
		}

		ps.Subscribe("topic-1", handler)

		err := ps.Publish("topic-1", msg)
		assert.NoError(t, err)
	})

	t.Run("should return timeout error when publishing", func(t *testing.T) {
		defer leaktest.Check(t)()

		ps := NewPubSub(cfg)

		handler := testHandler{
			name: "test",
			fn: func(ctx context.Context, ev Event) {
				time.Sleep(time.Millisecond * 400)
				ev.Ack()
			},
		}

		ps.Subscribe("topic-1", handler)

		err := ps.Publish("topic-1", map[string]string{"name": "Test"})
		assert.Error(t, err)
		assert.True(t, errors.Is(err, ErrPublishTimeout))
	})

	t.Run("should return invalid topic error when there aren't subscribers", func(t *testing.T) {
		ps := NewPubSub(cfg)

		err := ps.Publish("topic-1", map[string]string{"name": "Test"})
		assert.Error(t, err)
		assert.True(t, errors.Is(err, ErrInvalidTopic))
	})

	t.Run("should publish event to different topics", func(t *testing.T) {
		defer leaktest.Check(t)()

		ps := NewPubSub(cfg)

		msgTopic1 := map[string]string{"name": "topic-1"}
		msgTopic2 := map[string]string{"name": "topic-2"}

		ps.Subscribe("topic-1", testHandler{
			name: "test-topic-1",
			fn: func(ctx context.Context, ev Event) {
				assert.Equal(t, ev.Data, msgTopic1)
				ev.Ack()
			},
		})
		ps.Subscribe("topic-2", testHandler{
			name: "test-topic-2",
			fn: func(ctx context.Context, ev Event) {
				assert.Equal(t, ev.Data, msgTopic2)
				ev.Ack()
			},
		})

		err := ps.Publish("topic-1", msgTopic1)
		assert.NoError(t, err)

		err = ps.Publish("topic-2", msgTopic2)
		assert.NoError(t, err)
	})

	t.Run("should publish event to different handlers", func(t *testing.T) {
		defer leaktest.Check(t)()

		ps := NewPubSub(cfg)

		msgTopic1 := map[string]string{"name": "topic-1"}

		ps.Subscribe("topic-1", testHandler{
			name: "test-topic-1",
			fn: func(ctx context.Context, ev Event) {
				assert.Equal(t, ev.Data, msgTopic1)
				ev.Ack()
			},
		})
		ps.Subscribe("topic-1", testHandler{
			name: "test-topic-1",
			fn: func(ctx context.Context, ev Event) {
				time.Sleep(time.Millisecond * 400)
				ev.Ack()
			},
		})

		err := ps.Publish("topic-1", msgTopic1)
		assert.Error(t, err)
	})
}

type testHandler struct {
	name string
	fn   func(ctx context.Context, ev Event)
}

func (th testHandler) Name() string                         { return th.name }
func (th testHandler) Handle(ctx context.Context, ev Event) { th.fn(ctx, ev) }
