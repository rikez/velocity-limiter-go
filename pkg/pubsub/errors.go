package pubsub

import "errors"

var (
	// ErrInvalidTopic error returned when topic doesn't exist or doesn't have any subscriptions
	ErrInvalidTopic = errors.New("invalid topic")
	// ErrPublishTimeout error returned when a publish operation times out
	ErrPublishTimeout = errors.New("publish timeout")
)
