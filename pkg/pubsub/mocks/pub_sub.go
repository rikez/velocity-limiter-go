// Code generated by mockery v0.0.0-dev. DO NOT EDIT.

package pubsubmock

import (
	mock "github.com/stretchr/testify/mock"
	pubsub "gitlab.com/rikez/velocity-limiter-go.git/pkg/pubsub"
)

// PubSub is an autogenerated mock type for the PubSub type
type PubSub struct {
	mock.Mock
}

// Publish provides a mock function with given fields: topic, msg
func (_m *PubSub) Publish(topic string, msg interface{}) error {
	ret := _m.Called(topic, msg)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, interface{}) error); ok {
		r0 = rf(topic, msg)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Subscribe provides a mock function with given fields: topic, fn
func (_m *PubSub) Subscribe(topic string, fn pubsub.Handler) {
	_m.Called(topic, fn)
}

// Subscribers provides a mock function with given fields: topic
func (_m *PubSub) Subscribers(topic string) int {
	ret := _m.Called(topic)

	var r0 int
	if rf, ok := ret.Get(0).(func(string) int); ok {
		r0 = rf(topic)
	} else {
		r0 = ret.Get(0).(int)
	}

	return r0
}
