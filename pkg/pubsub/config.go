package pubsub

import "time"

// Config defines settings for the pubsub
type Config struct {
	Publisher PublisherConfig
}

// PublisherConfig defines settings for the publisher
type PublisherConfig struct {
	Timeout time.Duration `default:"10s"`
}
